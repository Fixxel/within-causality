﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vektron : MonoBehaviour {

    float findTimer;
    int HP;

    float stayLocation;

	void Start () {
        findTimer = 0;
        HP = 3;

        stayLocation = Random.Range(90,121);
	}
	
	// Update is called once per frame
	void Update () {
		if(gameObject.transform.position.z > stayLocation)
        {
            transform.Translate(Vector3.forward * 45 * Time.deltaTime);

            GameObject fp = GameObject.Find("PlayerContainer");
            transform.LookAt(fp.transform.position);
        }

        else if (gameObject.transform.position.z <= stayLocation)
        {
            GameObject fp = GameObject.Find("PlayerContainer");

            //Vector3 lookLoc = new Vector3(fp.transform.position.x, transform.position.y, fp.transform.position.z);
            transform.LookAt(fp.transform.position);
        }

        if (HP <= 0)
        {
            //giving score to player
            GameObject load = GameObject.Find("Score");
            load.GetComponent<Scoredisplay>().Score += 35;

            //explosion effect
            GameObject load1 = Resources.Load("effects/VektronExplosion") as GameObject;
            GameObject loaded1 = Instantiate(load1) as GameObject;

            loaded1.transform.position = transform.position;

            //dropping loot
            GameObject ges = GameObject.Find("GeneralEventSystem");
            ges.GetComponent<GeneralEvents>().DropLoot(Random.Range(0, 101), "vektron", transform.position);

            BroadcastMessage("MainBodyDestroyed");
            Destroy(gameObject);
        }
	}

    void WingDestroyed()
    {
        HP--;
    }

}
