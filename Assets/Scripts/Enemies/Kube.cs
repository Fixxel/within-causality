﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kube : MonoBehaviour {

	float fireTimer;
    float stayLocation;
    float stayTimer;

    public bool highlighted;
	public int HP;
    public string MovementBehaviour;

	// Use this for initialization
	void Start () {
		HP = 3;
		fireTimer = 1;

        highlighted = false;

        stayLocation = Random.Range(80,111);

        if (MovementBehaviour == "formation")
        {
            stayLocation = 600;
        }

		name = "Kube";

        //adding container as a parent for easier enemy mass destruction
        GameObject findparent = GameObject.Find("EnemyContainer");
        transform.parent = findparent.gameObject.transform;
    }
	
	// Update is called once per frame
	void Update () {
		fireTimer -= Time.deltaTime;
		gameObject.transform.Rotate (Random.Range(0,6),Random.Range(0,6),Random.Range(0,6) * Time.deltaTime);
        
		//moving behaviour when far away
		if(gameObject.transform.position.z > stayLocation)
        {
            gameObject.transform.position += new Vector3(0, 0, -100 * Time.deltaTime);
		}

        //movement behaviour when at staylocation + variations
        else if (gameObject.transform.position.z <= stayLocation)
        {
            if (MovementBehaviour == "aimingPlayer")
            {
                GameObject findplayership = GameObject.Find("PlayerShip");

                Vector3 lockZ = transform.position;
                lockZ.z = stayLocation;

                gameObject.transform.position = lockZ;

                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, findplayership.transform.position, 80f * Time.deltaTime);
            }
            else if (MovementBehaviour == "forwardSlow")
            {
                gameObject.transform.position += new Vector3(0, 0, -20 * Time.deltaTime);
            }
            else if (MovementBehaviour == "stayAWhile")
            {
                if (stayTimer > 5)
                {
                    gameObject.transform.position += new Vector3(0,0,-40 * Time.deltaTime);
                }
                stayTimer += Time.deltaTime;
            }
            else if (MovementBehaviour == "formation")
            {
                gameObject.transform.position += new Vector3(0, 0, -30 * Time.deltaTime);
            }
        }
		//firing behaviour when far away
		if(fireTimer <= 0 && gameObject.transform.position.z > stayLocation)
        {
			GameObject load = Resources.Load ("enemies/projectiles/KubeProjectile") as GameObject;
			GameObject KubeProjectile = Instantiate (load) as GameObject;

			KubeProjectile.transform.position = new Vector3 (gameObject.transform.position.x,gameObject.transform.position.y,gameObject.transform.position.z - 10);
			KubeProjectile.transform.name = "KubeProjectile";

			fireTimer = 4f;
		}
		//firing behaviour when close
		else if(fireTimer <= 0 && gameObject.transform.position.z <= stayLocation)
        {
			GameObject load = Resources.Load ("enemies/projectiles/KubeProjectile") as GameObject;
			GameObject KubeProjectile = Instantiate (load) as GameObject;

			KubeProjectile.transform.position = new Vector3 (gameObject.transform.position.x,gameObject.transform.position.y,gameObject.transform.position.z - 10);
			KubeProjectile.transform.name = "KubeProjectile";

			fireTimer = 2;
		}

        //destroying this when far behind player
        if(transform.position.z < -100){
            Destroy(gameObject);
        }

		//HP system
		if(HP <= 0){
            //giving score to player
            GameObject load = GameObject.Find("Score");
            load.GetComponent<Scoredisplay>().Score += 20;

            //loading explosion effect
            GameObject loadExplosion = Resources.Load("effects/KubeExplosion") as GameObject;
			GameObject Explosion = Instantiate (loadExplosion) as GameObject;

			Explosion.transform.position = gameObject.transform.position;
            
            //dropping loot, maybe
            GameObject ges = GameObject.Find("GeneralEventSystem");
            ges.GetComponent<GeneralEvents>().DropLoot(Random.Range(0,101),"Kube",transform.position);
            
            Destroy (gameObject);
			Destroy (this);
		}

        //highlighting when aimline hits this enemy
		transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;

        if (highlighted)
		{
			transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
		}

		highlighted = false;
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.transform.name == "PlayerContainer")
        {
            col.transform.GetComponent<Player>().PlayerHP--;
            HP--;
        }
    }
}
