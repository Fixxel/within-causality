﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkarabProjectile : MonoBehaviour {

    float timer;

	// Use this for initialization
	void Start () {
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (timer <= 1)
        {
            gameObject.transform.Translate(Vector3.forward * 5 * Time.deltaTime);
        }
        else if (timer > 1)
        {
            GameObject ps = GameObject.Find("PlayerShip");
            transform.position = Vector3.MoveTowards(transform.position, ps.gameObject.transform.position, 10f * Time.deltaTime);
            transform.position += new Vector3(0,0,-45f * Time.deltaTime);
        }

        gameObject.transform.Rotate(Vector3.back * 300f * Time.deltaTime);

        timer += Time.deltaTime;

        if (transform.position.z < -50)
        {
            Destroy(gameObject);
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "PlayerContainer")
        {
            col.gameObject.GetComponent<Player>().PlayerHP--;
            Destroy(gameObject);
        }
        
    }
}
