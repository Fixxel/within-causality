﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skarab : MonoBehaviour {

    //
    //NOTE FOR THIS ENEMY:
    //MELEE ATTACK DAMAGE COLLISION DETECTION AND PLAYER DAMAGING HAPPENS IN PLAYER SCRIPT! NOT HERE!
    //(sorry future me if this causes headaches later...)
    //
    public bool highlighted = false;

	bool charging = false;
	bool returning = false;

	float rotateVelocity = 20f;
	float firingTimer = 0f;
	float behaviourTimer = 0f;
    float destructionTimer = 0f;

    float stayLocation;

	public int HP = 550;
	int num6 = 6;

	// Use this for initialization
	void Start () {
        name = "Skarab";
		HP = 50;

        stayLocation = Random.Range(55,81);
	}
	
	// Update is called once per frame
	void Update () {
		GameObject ps = GameObject.Find ("PlayerShip");

		//movement
		if(gameObject.transform.position.z > stayLocation)
        {
			gameObject.transform.position += new Vector3 (0,0,-100f * Time.deltaTime);
		}
		else if(gameObject.transform.position.z <= stayLocation)
        {
			gameObject.transform.Rotate (Vector3.right * rotateVelocity * Time.deltaTime);
			//gameObject.transform.Rotate (Vector3.up * rotateVelocity * Time.deltaTime);
		}

		//phases
		//firing behaviour
		if(behaviourTimer <= 20){
			//firing
			if(firingTimer <= 0 && rotateVelocity >= 30f && transform.position.z <= 80){
                GameObject load = Resources.Load("enemies/projectiles/SkarabProjectile") as GameObject;
                GameObject loaded = Instantiate(load) as GameObject;

                loaded.transform.position = transform.position;
                loaded.transform.position += new Vector3(3f,0,0);
                loaded.transform.eulerAngles = new Vector3(0,90f,0);

                GameObject load2 = Resources.Load("enemies/projectiles/SkarabProjectile") as GameObject;
                GameObject loaded2 = Instantiate(load2) as GameObject;

                loaded2.transform.position = transform.position;
                loaded2.transform.position += new Vector3(-3f, 0, 0);
                loaded2.transform.eulerAngles = new Vector3(0, -90f, 0);

                firingTimer = 1;
			}
		}
		//charging behaviour
		else if(behaviourTimer > 20 && behaviourTimer <= 40){
			//aiming the charge
			if(behaviourTimer > 20 && behaviourTimer <= 22 && HP > 0){
				Vector3 lockZ = transform.position;
				lockZ.z = 40;

				gameObject.transform.position = lockZ;

				gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, ps.transform.position, 140f * Time.deltaTime);
			}
			//charging
			if (behaviourTimer > 22 && behaviourTimer < 23 && HP > 0){
				rotateVelocity += 10 * Time.deltaTime;
				gameObject.transform.position += new Vector3 (0,0,-100f * Time.deltaTime);

				//melee spiral colliders & appearance enabling
				var melee = gameObject.transform.GetChild (2);

				melee.GetComponent<ParticleSystem> ().Play();

				melee.GetChild (0).GetComponent<ParticleSystem> ().Play();
				melee.GetChild (1).GetComponent<ParticleSystem> ().Play();
				melee.GetChild (2).GetComponent<ParticleSystem> ().Play();
				melee.GetChild (3).GetComponent<ParticleSystem> ().Play();
				melee.GetChild (4).GetComponent<ParticleSystem> ().Play();

				melee.GetChild (5).GetComponent<CapsuleCollider> ().enabled = true;
				melee.GetChild (6).GetComponent<CapsuleCollider> ().enabled = true;
				melee.GetChild (7).GetComponent<CapsuleCollider> ().enabled = true;
			}
			//returning and resetting
			else if(behaviourTimer > 23 && HP > 0){
				rotateVelocity = 20;
				gameObject.transform.position += new Vector3 (0,0,100f * Time.deltaTime);
				if(gameObject.transform.position.z >= stayLocation)
                {
					behaviourTimer = 0;

					//melee spiral colliders & appearance disabling
					var melee = gameObject.transform.GetChild (2);

					melee.GetComponent<ParticleSystem> ().Stop();

                    melee.GetChild(0).GetComponent<ParticleSystem>().Stop();
					melee.GetChild (1).GetComponent<ParticleSystem> ().Stop();
					melee.GetChild (2).GetComponent<ParticleSystem> ().Stop();
					melee.GetChild (3).GetComponent<ParticleSystem> ().Stop();
					melee.GetChild (4).GetComponent<ParticleSystem> ().Stop();

					melee.GetChild (5).GetComponent<CapsuleCollider> ().enabled = false;
					melee.GetChild (6).GetComponent<CapsuleCollider> ().enabled = false;
					melee.GetChild (7).GetComponent<CapsuleCollider> ().enabled = false;
				}
			}
		}

        if (transform.position.z <= 80 && HP > 0)
        {
            firingTimer -= Time.deltaTime;
            behaviourTimer += 1 * Time.deltaTime;
        }
		rotateVelocity += 10 * Time.deltaTime;

		//HP
		if(HP <= 0){

            if (destructionTimer == 0)
            {
                GameObject load = Resources.Load("effects/SkarabDestruction") as GameObject;
                GameObject loaded = Instantiate(load) as GameObject;

                loaded.transform.position = transform.position;
                loaded.transform.SetParent(gameObject.transform);
            }
            
            transform.position += new Vector3(0, 0, -10f * Time.deltaTime);

            transform.Rotate(Vector3.forward * Random.Range(50, 101) * Time.deltaTime);
            transform.Rotate(Vector3.up * Random.Range(50, 101) * Time.deltaTime);

            destructionTimer += Time.deltaTime;

            if (destructionTimer >= 3)
            {
                //giving score to player
                GameObject load = GameObject.Find("Score");
                load.GetComponent<Scoredisplay>().Score += 500;

                //dropping loot
                GameObject ges = GameObject.Find("GeneralEventSystem");
                ges.GetComponent<GeneralEvents>().DropLoot(Random.Range(0, 101), "Skarab", transform.position);
                
                //detaching child (explosion) so it doesnt just disappear with Skarab
                gameObject.transform.GetChild(4).transform.parent = null;
                
                Destroy(gameObject);
                Destroy(this);
            }
		}

        //highlighting when aimline hits this enemy

		transform.GetChild(3).GetComponent<MeshRenderer>().enabled = false;

		if (highlighted)
		{
			transform.GetChild(3).GetComponent<MeshRenderer>().enabled = true;
		}

		highlighted = false;
    }


}
