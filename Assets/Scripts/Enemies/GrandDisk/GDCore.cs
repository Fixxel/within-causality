﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GDCore : MonoBehaviour {

	float spawnTimer = 0;
	float frequentFireTimer = 0;
	float infrequentFireTimer = 0;
	float cooldownTimer = -30f;

	public int SectionsDestroyed = 0;
	public int HP = 30;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		Debug.Log (SectionsDestroyed);

		gameObject.transform.Rotate (Vector3.back * 50 * Time.deltaTime);
		gameObject.transform.Rotate (Vector3.up * 20 * Time.deltaTime);


		//increasing rotation per layer (of segments) destroyed
		if(SectionsDestroyed >= 4){ //layer 5
			gameObject.transform.Rotate (Vector3.back * 25 * Time.deltaTime);
			gameObject.transform.Rotate (Vector3.up * 10 * Time.deltaTime);

			if(SectionsDestroyed >= 7){ //layer 4
				gameObject.transform.Rotate (Vector3.back * 25 * Time.deltaTime);
				gameObject.transform.Rotate (Vector3.up * 10 * Time.deltaTime);

				if(SectionsDestroyed >= 10){ //layer 3
					gameObject.transform.Rotate (Vector3.back * 25 * Time.deltaTime);
					gameObject.transform.Rotate (Vector3.up * 10 * Time.deltaTime);

					if(SectionsDestroyed >= 11){ //layer 2
						gameObject.transform.Rotate (Vector3.back * 25 * Time.deltaTime);
						gameObject.transform.Rotate (Vector3.up * 10 * Time.deltaTime);

						if(SectionsDestroyed >= 12){ //layer 1
							gameObject.transform.Rotate (Vector3.back * 25 * Time.deltaTime);
							gameObject.transform.Rotate (Vector3.up * 10 * Time.deltaTime);
						}
					}
				}
			}
		}

		//timer increases here
		spawnTimer += Time.deltaTime;
		frequentFireTimer += Time.deltaTime;
		infrequentFireTimer += Time.deltaTime;
		cooldownTimer += Time.deltaTime;

		if(cooldownTimer >= 0){
			frequentFireTimer = -30f;
			infrequentFireTimer = -30f;

			cooldownTimer = -60f;
		}

		//BEHAVIOUR
		//spawning behaviour - will spawn Disks
		if(spawnTimer >= Random.Range(1,5)){
			SpawnDisk ();
			spawnTimer = 0f;
		}

		//frequent firing behaviour
		if(frequentFireTimer >= 3f){
			GameObject load = Resources.Load ("enemies/projectiles/GDcoreChargedProjectile") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.transform.position = gameObject.transform.position;


			frequentFireTimer = 0;
		}
		//infrequent firing behaviour
		if(infrequentFireTimer >= Random.Range(5,10)){
			GameObject load = Resources.Load ("enemies/projectiles/GDcoreChargedProjectile") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.transform.position = gameObject.transform.position;


			infrequentFireTimer = 0;
		}

		if(HP <= 0){
			GameObject load = GameObject.Find ("GDcore");
			GDCore loaded = load.GetComponent<GDCore> ();

			GameObject load2 = GameObject.Find ("GrandDisk");

			loaded.SectionsDestroyed++;

			Destroy (load2);
			Destroy (gameObject);
			Destroy (this);
		}
	}

	void OnCollisionEnter(Collision Col){
		GameObject load = GameObject.Find ("GDcore");
		GDCore loaded = load.GetComponent<GDCore> ();

		if(Col.gameObject.name == "Laser" && loaded.SectionsDestroyed >= 12){
			HP--;

			GameObject load2 = Resources.Load ("effects/Debris") as GameObject;
			GameObject loaded2 = Instantiate (load2) as GameObject;

			loaded2.gameObject.transform.position = Col.gameObject.transform.position;
		}
		else if(Col.gameObject.name == "Laser" && loaded.SectionsDestroyed < 12){

			GameObject load2 = Resources.Load ("effects/GDshield") as GameObject;
			GameObject loaded2 = Instantiate (load2) as GameObject;

			loaded2.gameObject.transform.position = Col.gameObject.transform.position;
		}

	}

	void SpawnDisk(){
		GameObject load = Resources.Load ("enemies/Disk") as GameObject;
		GameObject loaded = Instantiate (load) as GameObject;

		if(cooldownTimer <= -30f){
			GameObject load2 = Resources.Load ("effects/GDshield") as GameObject;
			GameObject loaded2 = Instantiate (load2) as GameObject;

			loaded2.gameObject.transform.position = gameObject.transform.position;
		}

		loaded.gameObject.transform.position = gameObject.transform.position;
		loaded.gameObject.name = "Disk";
	}

	public void SectionDestroyed(){
		GameObject load = Resources.Load ("effects/GDdamaged") as GameObject;
		GameObject loaded = Instantiate (load) as GameObject;

		loaded.gameObject.transform.position = gameObject.transform.position;
	}

}
