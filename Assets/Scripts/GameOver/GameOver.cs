﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	string selected = "NewGame";

	// Use this for initialization
	void Start () {
        GameObject findnewgame = GameObject.Find("NewGame");
		findnewgame.GetComponent<Animator> ().enabled = true;

        GameObject findScore = GameObject.Find("Score");
        findScore.transform.position = new Vector3(-30,20,40);
        findScore.transform.eulerAngles = new Vector3(0,0,0);
	}
	
	// Update is called once per frame
	void Update () {
        GameObject findng = GameObject.Find("NewGame");
        GameObject findmm = GameObject.Find("MainMenu");

        if (Input.GetKey(KeyCode.Space) || Input.GetButtonDown("Button A")){
			if(selected == "NewGame"){
                GameObject deleteLastGameScore = GameObject.Find("Score");
                Destroy(deleteLastGameScore);

                SceneManager.LoadScene ("Solo");
			}
            else if (selected == "MainMenu")
            {
                GameObject deleteLastGameScore = GameObject.Find("Score");
                Destroy(deleteLastGameScore);

                SceneManager.LoadScene("MainMenu");
            }
		}
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (selected == "NewGame")
            {
                selected = "MainMenu";
                findmm.GetComponent<Animator>().enabled = true;
                findng.GetComponent<Animator>().enabled = false;
            }
            else if (selected == "MainMenu")
            {
                selected = "NewGame";
                findng.GetComponent<Animator>().enabled = true;
                findmm.GetComponent<Animator>().enabled = false;
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (selected == "NewGame")
            {
                selected = "MainMenu";
                findmm.GetComponent<Animator>().enabled = true;
                findng.GetComponent<Animator>().enabled = false;
            }
            else if (selected == "MainMenu")
            {
                selected = "NewGame";
                findng.GetComponent<Animator>().enabled = true;
                findmm.GetComponent<Animator>().enabled = false;
            }
        }
	}
}
