﻿using UnityEngine;
using System.Collections;

public class CurrentWeaponDisplay : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Brings playerscript for editing textmesh tied to player
        GameObject load = GameObject.Find("PlayerContainer");
        Player ps = load.GetComponent<Player>();

        if (GetComponent<TextMesh>().text != ps.PlayerWeapon)
        {
            GetComponent<TextMesh>().text = ps.PlayerWeapon.ToUpper();
        }
    }
}
