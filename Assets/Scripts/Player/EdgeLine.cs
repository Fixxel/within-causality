﻿using UnityEngine;
using System.Collections;

public class EdgeLine : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<LineRenderer>().material = new Material(Shader.Find("Particles/Additive"));
    }

    // Update is called once per frame
    void Update()
    {
        GameObject load = GameObject.Find("PlayerShip");
        
        GetComponent<LineRenderer>().SetPosition(0, transform.position);
        GetComponent<LineRenderer>().SetPosition(1, load.transform.position);

        transform.position = new Vector3(load.transform.position.x, load.transform.position.y, 40);

        if (load.transform.position.z < 0)
        {
            GetComponent<TextMesh>().color = new Color(0,0.6f,0.9f,0.1f);
        }
        if (load.transform.position.z > 0)
        {
            GetComponent<TextMesh>().color = new Color(0, 0.6f, 0.9f, 0.4f);
        }
    }
}