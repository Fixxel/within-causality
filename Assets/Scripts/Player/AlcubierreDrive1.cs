﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlcubierreDrive1 : MonoBehaviour {

	int rotationRate = 200;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (Vector3.back * rotationRate * Time.deltaTime);
		gameObject.transform.Rotate (Vector3.left * rotationRate/Random.Range(4,6) * Time.deltaTime);
		gameObject.transform.Rotate (Vector3.down * rotationRate/Random.Range(4,6) * Time.deltaTime);

		//for increasing rotationRate when player is evading
		GameObject player = GameObject.Find ("PlayerContainer");
		Player _pscrit = player.GetComponent<Player>();

		if(_pscrit.PlayerEvading == true){
			rotationRate = 800;
		}else if(_pscrit.PlayerEvading == false && rotationRate != 200){
			rotationRate = 200;
		}
	}
}
