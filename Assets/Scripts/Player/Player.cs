﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public bool PlayerEvading;
	public bool PlayerMouseAiming;
    public bool PlayerControllerAiming;

    bool AimLineHit;
    public bool ReleasingHeat;

	public float EvasionRecovery;
	public float EvasionDuration;
	public float PlayerSpeed;
	public float heat;
    public float heatManagement;
    float heatDamageTimer;

    public int PlayerHP;
    int PlayerHPmax;

    int weaponNum;

	public string PlayerWeapon;

	// Use this for initialization
	void Start () {
        
        weaponNum = 1;

        heatManagement = 4;

        PlayerHP = 20;
        PlayerHPmax = 20;

        PlayerSpeed = 30;
	}


	// Update is called once per frame
	void Update () {
        GameObject player = GameObject.Find("PlayerShip");
        GameObject maincamera = GameObject.Find("Main Camera");
        GameObject crosshair = GameObject.Find("Crosshair");
        GameObject heatCircle = GameObject.Find("heatCircle");

        //capping max hp
        if (PlayerHP > PlayerHPmax)
        {
            PlayerHP = PlayerHPmax;
        }

        //lowering heat if there is any - and damaging over time if there is too much (over 100)
        if (heatCircle.GetComponent<Image>().fillAmount > 0)
        {
            if (heatCircle.GetComponent<Image>().fillAmount > 0.5f)
            {
                heatDamageTimer += Time.deltaTime;

                if (heatDamageTimer > 7)
                {
                    PlayerHP--;
                    heatDamageTimer = 0;
                }
            }
            else if (heatCircle.GetComponent<Image>().fillAmount < 0.5f)
            {
                heatDamageTimer = 0;
            }
        }
        //releasing heat system
        if (Input.GetKeyDown(KeyCode.R) && heatCircle.GetComponent<Image>().fillAmount > 0.5 || Input.GetButton("Button Y") && heatCircle.GetComponent<Image>().fillAmount > 0.5 || Input.GetButton("Button X") && heatCircle.GetComponent<Image>().fillAmount > 0.5f)
        {
            ReleasingHeat = true;
            PlayerSpeed = 10;
        }

        if (ReleasingHeat == true)
        {
            GameObject load = Resources.Load("effects/heatReleaseEffect") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = transform.position;

            if (heatCircle.GetComponent<Image>().fillAmount < 0.5f)
            {
                PlayerSpeed = 30;
                ReleasingHeat = false;
            }
        }

        //reset solo
		//if(Input.GetKeyDown(KeyCode.B)){
  //          SceneManager.LoadScene ("Solo");
		//}
        
        //for testing HP UI
        if (Input.GetKeyDown(KeyCode.I))
        {
            PlayerHP++;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            PlayerHP--;
        }

        //AIMING LINE AND HANDLING IF IT HITS, IT HIGHLIGHTS ENEMY
        RaycastHit hit;
        Ray aimlineRay = new Ray(transform.position, crosshair.transform.position);

        if (Physics.Raycast(aimlineRay, out hit))
        {
            if (hit.collider.tag != "PlayerProjectile" && hit.collider.tag != "EnemyProjectile" && hit.collider.tag != "EnvironmentCollider" && hit.collider.gameObject.transform.position.z > transform.position.z+20 && !PlayerMouseAiming)
            {
                crosshair.GetComponent<LineRenderer>().SetPosition(0, transform.position);
                crosshair.GetComponent<LineRenderer>().SetPosition(1, new Vector3(crosshair.transform.position.x, crosshair.transform.position.y, hit.collider.gameObject.transform.position.z));
				crosshair.GetComponent<LineRenderer>().startColor = Color.red;
                crosshair.GetComponent<LineRenderer>().endColor = Color.red;
                crosshair.GetComponent<LineRenderer>().startWidth = 0.5f;
                crosshair.GetComponent<LineRenderer>().endWidth = 0.5f;

                crosshair.GetComponent<Crosshair>().lineObscrured = true;

				switch (hit.collider.tag)
				{
					case "SkarabCollider":
						hit.collider.gameObject.transform.GetComponent<Skarab>().highlighted = true;
						break;
					case "KubeCollider":
						hit.collider.gameObject.transform.GetComponent<Kube>().highlighted = true;
						break;
					case "DiskCollider":
						hit.collider.gameObject.transform.GetComponent<Disk>().highlighted = true;
						break;
					case "SchtrykerCollider":
						hit.collider.gameObject.transform.GetComponent<Schtryker>().highlighted = true;
						break;
                    case "VektronWing1":
                        hit.collider.gameObject.transform.GetComponent<VektronWing>().highlighted = true;
                        break;
                    case "VektronWing2":
                        hit.collider.gameObject.transform.GetComponent<VektronWing>().highlighted = true;
                        break;
                    case "VektronWing3":
                        hit.collider.gameObject.transform.GetComponent<VektronWing>().highlighted = true;
                        break;
                    case "VektronWing4":
                        hit.collider.gameObject.transform.GetComponent<VektronWing>().highlighted = true;
                        break;
                    case "Cield":
                        hit.collider.gameObject.transform.GetComponent<VektronWing>().highlighted = true;
                        break;
                }

            }
        }

        //CONTROLS
        //A - LEFT
        if (Input.GetKey(KeyCode.A) || Input.GetAxis("Horizontal") < 0){
			//movement
			transform.Translate (Vector3.left * PlayerSpeed * Time.deltaTime);

			//camera & rotation
			if(gameObject.transform.position.x > -30){
				player.transform.Rotate (0, 0, PlayerSpeed/30);
		        maincamera.transform.Translate (Vector3.right * PlayerSpeed/8 * Time.deltaTime);
			}
		}
        //aiming
        if (Input.GetAxis("Second Horizontal") < 0 && PlayerControllerAiming == true)
        {
            crosshair.transform.Translate(Vector3.left * 300 * Time.deltaTime);
            player.transform.LookAt(crosshair.transform.position);
        }

        //D - RIGHT
		if(Input.GetKey(KeyCode.D) || Input.GetAxis("Horizontal") > 0){
			//movement
			transform.Translate (Vector3.right * PlayerSpeed * Time.deltaTime);

			//camera & rotation
			if(gameObject.transform.position.x < 30){
				player.transform.Rotate (0, 0, -PlayerSpeed/30);
				maincamera.transform.Translate (Vector3.left * PlayerSpeed/8 * Time.deltaTime);
			}
        }
        //aiming
        if (Input.GetAxis("Second Horizontal") > 0 && PlayerControllerAiming == true)
        {
            crosshair.transform.Translate(Vector3.right * 300 * Time.deltaTime);
            player.transform.LookAt(crosshair.transform.position);
        }

        //W - UP
        if (Input.GetKey(KeyCode.W) || Input.GetAxis("Vertical") > 0){
			//movement
			transform.Translate (Vector3.up * PlayerSpeed * Time.deltaTime);

			//camera & rotation
			if(gameObject.transform.position.y < 30){
				player.transform.Rotate(0, 0, PlayerSpeed/30);
				maincamera.transform.Translate (Vector3.down * PlayerSpeed/8 * Time.deltaTime);
			}
        }
        //aiming
        if (Input.GetAxis("Second Vertical") < 0 && PlayerControllerAiming == true)
        {
            crosshair.transform.Translate(Vector3.down * 300 * Time.deltaTime);
            player.transform.LookAt(crosshair.transform.position);
        }

        //S - DOWN
        if(Input.GetKey(KeyCode.S) || Input.GetAxis("Vertical") < 0){
			//movement
			transform.Translate (Vector3.down * PlayerSpeed * Time.deltaTime);

			//camera & rotation
			if(gameObject.transform.position.y > -30){
				player.transform.Rotate(0, 0, -PlayerSpeed/30);
				maincamera.transform.Translate (Vector3.up * PlayerSpeed/8 * Time.deltaTime);
			}
        }
        //aiming
        if (Input.GetAxis("Second Vertical") > 0 && PlayerControllerAiming == true)
        {
            crosshair.transform.Translate(Vector3.up * 300 * Time.deltaTime);
            player.transform.LookAt(crosshair.transform.position);
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0 || Input.GetAxis("D-pad Vertical") > 0)    //mousewheel/keypad up
        {
            transform.Translate(Vector3.forward * 3 * PlayerSpeed * Time.deltaTime);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0 || Input.GetAxis("D-pad Vertical") < 0)    //mousewheel/keypad down
        {
            transform.Translate(Vector3.back * 3 *  PlayerSpeed * Time.deltaTime);
        }

		//evasion input
		if(Input.GetKeyDown(KeyCode.E) && !ReleasingHeat || Input.GetMouseButtonDown(2) && !ReleasingHeat || Input.GetButton("Button B") && !ReleasingHeat || Input.GetButton("Button A") && !ReleasingHeat){
			if(PlayerEvading == false && EvasionRecovery <= 0){
				PlayerEvading = true;
				PlayerSpeed += PlayerSpeed*2;
				EvasionDuration = 2f;
			}
		} //evasion mechanism
		if(PlayerEvading == true){
			if (gameObject.transform.position.x > 0 && gameObject.transform.position.x <= 30) {
				player.transform.Rotate (Vector3.forward * 360 * Time.deltaTime);
			}else if(gameObject.transform.position.x >= -30 && gameObject.transform.position.x <= 0){
				player.transform.Rotate (Vector3.back * 360 * Time.deltaTime);
			}

			EvasionDuration -= Time.deltaTime;

			if(EvasionDuration <= 0){
				PlayerEvading = false;

				PlayerSpeed = PlayerSpeed / 3;

				EvasionRecovery = 0.5f;
			}
		}

		//recovering from evasion
		if(EvasionRecovery > 0){
			EvasionRecovery -= Time.deltaTime;
		}

		//DIRECTIONAL RESTRICTION
		//X direction restrictions
		if(gameObject.transform.position.x > 30){
			transform.position = new Vector3 (30, gameObject.transform.position.y, gameObject.transform.position.z);
		}
		if(gameObject.transform.position.x < -30)
        {
			transform.position = new Vector3 (-30, gameObject.transform.position.y, gameObject.transform.position.z);
		}
		//Y direction restrictions
		if(gameObject.transform.position.y > 30)
        {
			transform.position = new Vector3 (gameObject.transform.position.x, 30, gameObject.transform.position.z);
		}
		if(gameObject.transform.position.y < -30)
        {
			transform.position = new Vector3 (gameObject.transform.position.x, -30, gameObject.transform.position.z);
		}
		//Z direction restrictions
		if(gameObject.transform.position.z > 30)
        {
			transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, 30);
		}
		if(gameObject.transform.position.z < -30)
        {
			transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, -30);
		}

		//HP SYSTEM - GAME OVER CHECK AND SCENE LOAD
		if(PlayerHP <= 0){
            GameObject score = GameObject.Find("Score");

            gameObject.transform.GetChild(0).transform.parent = null;
            DontDestroyOnLoad(score);

            SceneManager.LoadScene ("GameOver");
            BroadcastMessage("gameOver");
		}

		//UNLOCKING/LOCKING MOUSE AIMING MODE
		if(Input.GetKeyDown(KeyCode.Q)){
			if(PlayerMouseAiming == false){
				//releasing mouse aim
				PlayerMouseAiming = true;
			}else if(PlayerMouseAiming == true){
				//locking mouse aim
				PlayerMouseAiming = false;
				player.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, 1.0f * Time.deltaTime);
			}
		}
        //UNLOCKING/LOCKING CONTROLLER AIMING MODE
        if (Input.GetButtonDown("Right Stick Click"))
        {
            if (PlayerControllerAiming == false)
            {
                //releasing mouse aim
                PlayerControllerAiming = true;
                //downside is that speed halves
                //PlayerSpeed = PlayerSpeed / 3;
            }else if (PlayerControllerAiming == true)
            {
                //locking mouse aim
                PlayerControllerAiming = false;
                //PlayerSpeed += PlayerSpeed * 2;
                //upside is that speed doubles
                crosshair.transform.position = player.transform.position;
                crosshair.transform.position += new Vector3(0,0,600);
                //returning player rotation back to mouse aim locked
                player.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, 1.0f * Time.deltaTime);
            }
        }


		//ROTATING PLAYERSHIP TO CROSSHAIR
		if(PlayerMouseAiming == true){
			player.transform.LookAt (crosshair.transform.position);
		}

        //WEAPON SYSTEM INPUT
        //FIRING
        if(!Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0) || Input.GetAxis("Right Trigger") >= 1)
        {
            FireProjectile(heatCircle.GetComponent<Image>().fillAmount);
        }
        if (Input.GetKey(KeyCode.Space) && (Input.GetKey(KeyCode.LeftShift)))
        {
            FireSecondaryProjectile(heatCircle.GetComponent<Image>().fillAmount);
        }
        else if (Input.GetMouseButton(1))
        {
            FireSecondaryProjectile(heatCircle.GetComponent<Image>().fillAmount);
        }
        else if (Input.GetAxis("Left Trigger") >= 1)
        {
            FireSecondaryProjectile(heatCircle.GetComponent<Image>().fillAmount);
        }


        //CHANGING WEAPON WITH KEYBOARD NUMBERS
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            weaponNum = 1;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            weaponNum = 2;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            weaponNum = 3;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            weaponNum = 4;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            weaponNum = 5;
        }

        //CHANGING WEAPON WITH CONTROLLER LEFT AND RIGHT KEYS
        if (Input.GetButtonDown("Right Bumper"))
        {
            weaponNum++;
            if (weaponNum > 5)
            {
                weaponNum = 1;
            }
        }
        else if (Input.GetButtonDown("Left Bumper"))
        {
            weaponNum--;
            if (weaponNum < 1)
            {
                weaponNum = 5;
            }
        }

        //LISTENING FOR WEAPONNUM
        switch (weaponNum)
        {
            case 1:
                PlayerWeapon = "fiveshot";
                break;
            case 2:
                PlayerWeapon = "scattershot";
                break;
            case 3:
                PlayerWeapon = "railgun";
                break;
            case 4:
                PlayerWeapon = "loyalfire";
                break;
            case 5:
                PlayerWeapon = "shyfire";
                break;
        }


    }

	//WEAPON SYSTEM
    void FireProjectile(float heatC)
    {
        GameObject cooldownCircle = GameObject.Find("weaponCooldownCircle");

        if (cooldownCircle.GetComponent<Image>().fillAmount == 0 && heatC < 1 && !ReleasingHeat)
        {
            GameObject fp = GameObject.Find("FiringPoint");
            GameObject ps = GameObject.Find("PlayerShip");
            GameObject hc = GameObject.Find("heatCircle");

            GameObject load = Resources.Load("player/projectiles/Laser") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = fp.transform.position;
            loaded.transform.rotation = ps.transform.rotation;

            cooldownCircle.GetComponent<Image>().fillAmount += 0.10f;
            hc.GetComponent<Image>().fillAmount += 0.0015f;
        }
    }
	void FireSecondaryProjectile (float heatC)
    {
        GameObject fp = GameObject.Find("FiringPoint");
        GameObject ps = GameObject.Find("PlayerShip");
        GameObject hc = GameObject.Find("heatCircle");
        GameObject cooldownCircle = GameObject.Find("weaponCooldownCircle");

        //FIVESHOT - LASER VARIANT
        if (PlayerWeapon == "fiveshot" && heatC < 1 && cooldownCircle.GetComponent<Image>().fillAmount == 0 && !ReleasingHeat)
        {
            GameObject load = Resources.Load("player/projectiles/Fiveshot") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = fp.transform.position;
            loaded.transform.rotation = ps.transform.rotation;

            cooldownCircle.GetComponent<Image>().fillAmount += 0.3f;
            hc.GetComponent<Image>().fillAmount += 0.060f;
        }

		//SCATTERSHOT
		else if(PlayerWeapon == "scattershot" && heatC < 1 && cooldownCircle.GetComponent<Image>().fillAmount == 0 && !ReleasingHeat)
        {
			GameObject load = Resources.Load ("player/projectiles/Scattershot") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.transform.position = fp.transform.position;
			loaded.transform.rotation = ps.transform.rotation;

            cooldownCircle.GetComponent<Image>().fillAmount += 0.05f;
            hc.GetComponent<Image>().fillAmount += 0.015f;
        }

		//RAILGUN
		else if(PlayerWeapon == "railgun" && heatC < 1 && cooldownCircle.GetComponent<Image>().fillAmount == 0 && !ReleasingHeat)
        {
			GameObject load = Resources.Load ("player/projectiles/Railgun") as GameObject;
			GameObject loaded = Instantiate (load) as GameObject;

			loaded.transform.position = fp.transform.position;
			loaded.transform.rotation = ps.transform.rotation;

            cooldownCircle.GetComponent<Image>().fillAmount += 1f;
            hc.GetComponent<Image>().fillAmount += 0.300f;
        }

        //SHYFIRE
        else if (PlayerWeapon == "shyfire" && heatC < 1 && cooldownCircle.GetComponent<Image>().fillAmount == 0 && !ReleasingHeat)
        {
            GameObject load = Resources.Load("player/projectiles/Shyfire") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = fp.transform.position;
            loaded.transform.rotation = ps.transform.rotation;

            cooldownCircle.GetComponent<Image>().fillAmount += 0.4f;
            hc.GetComponent<Image>().fillAmount += 0.040f;
        }

        //LOYALFIRE
        else if (PlayerWeapon == "loyalfire" && heatC < 1 && cooldownCircle.GetComponent<Image>().fillAmount == 0 && !ReleasingHeat)
        {
            GameObject load = Resources.Load("player/projectiles/Loyalfire") as GameObject;
            GameObject loaded = Instantiate(load) as GameObject;

            loaded.transform.position = fp.transform.position;
            loaded.transform.rotation = ps.transform.rotation;

            cooldownCircle.GetComponent<Image>().fillAmount += 0.45f;
            hc.GetComponent<Image>().fillAmount += 0.055f;
        }

    }

	void OnCollisionEnter(Collision col){
		if(col.gameObject.name == "SkarabMeleeHitbox"){
			PlayerHP -= 1;
		}
	}
}
