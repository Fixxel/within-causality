﻿using UnityEngine;
using System.Collections;

public class BravefireProjectile : MonoBehaviour
{
    bool spawned = false;
    bool locked = false;

    Vector3 lastPos;

    float maxRange = 11;

    // Use this for initialization
    void Start()
    {
        name = "Shyfire";

        lastPos = transform.position;

        Destroy(gameObject, 30f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * 50f * Time.deltaTime);

        transform.Translate(Vector3.left * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.right * Random.Range(0, maxRange) * Time.deltaTime);

        transform.Translate(Vector3.up * Random.Range(0, maxRange) * Time.deltaTime);
        transform.Translate(Vector3.down * Random.Range(0, maxRange) * Time.deltaTime);

        RaycastHit hit;

        Ray thisCol = new Ray(lastPos, transform.position);
        

        if (Physics.SphereCast(transform.position, 10f, transform.position, out hit, Mathf.Infinity))
        {
            if (hit.collider.tag == "KubeCollider")
            {
                transform.position = Vector3.MoveTowards(transform.position, hit.collider.gameObject.transform.position, 400f * Time.deltaTime);
            }
            else if (hit.collider.tag == "DiskCollider")
            {
                transform.position = Vector3.MoveTowards(transform.position, hit.collider.gameObject.transform.position, 400f * Time.deltaTime);
            }
            else if (hit.collider.tag == "SkarabCollider")
            {
                transform.position = Vector3.MoveTowards(transform.position, hit.collider.gameObject.transform.position, 400f * Time.deltaTime);
            }
            else if (hit.collider.tag == "SchtrykerCollider")
            {
                transform.position = Vector3.MoveTowards(transform.position, hit.collider.gameObject.transform.position, 400f * Time.deltaTime);
            }
        }

        if (Physics.Raycast(thisCol, out hit))
        {
            if (hit.collider.tag == "KubeCollider")
            {
                hit.collider.GetComponent<Kube>().HP -= 25;
                Destroy(gameObject, 1f);
            }
            else if (hit.collider.tag == "DiskCollider")
            {
                hit.collider.GetComponent<Disk>().HP -= 25;
                Destroy(gameObject, 1f);
            }
            else if (hit.collider.tag == "SkarabCollider")
            {
                hit.collider.GetComponent<Skarab>().HP -= 25;
                Destroy(gameObject, 1f);
            }
            else if (hit.collider.tag == "SchtrykerCollider")
            {
                hit.collider.GetComponent<Schtryker>().HP -= 25;
                Destroy(gameObject, 1f);
            }
        }

        lastPos = transform.position;

    }
}
