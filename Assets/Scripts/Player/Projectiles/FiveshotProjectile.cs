﻿using UnityEngine;
using System.Collections;

public class FiveshotProjectile : MonoBehaviour
{
    int randRot;

    // Use this for initialization
    void Start()
    {
        randRot = Random.Range(0,2);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * 200 * Time.deltaTime);

        if (randRot == 0)
        {
            transform.eulerAngles += new Vector3(0, 0, Random.Range(50, 201) * Time.deltaTime);
        }else {
            transform.eulerAngles += new Vector3(0, 0, Random.Range(-50, -201) * Time.deltaTime);
        }
        

        transform.GetComponent<LineRenderer>().SetPosition(0, transform.GetChild(0).transform.position);

        transform.GetComponent<LineRenderer>().SetPosition(1, transform.GetChild(1).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(2, transform.GetChild(2).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(3, transform.GetChild(3).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(4, transform.GetChild(4).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(5, transform.GetChild(1).transform.position);

        transform.GetComponent<LineRenderer>().SetPosition(6, transform.GetChild(0).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(7, transform.GetChild(2).transform.position);

        transform.GetComponent<LineRenderer>().SetPosition(8, transform.GetChild(0).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(9, transform.GetChild(3).transform.position);

        transform.GetComponent<LineRenderer>().SetPosition(10, transform.GetChild(0).transform.position);
        transform.GetComponent<LineRenderer>().SetPosition(11, transform.GetChild(4).transform.position);

        
    }

    private void OnCollisionEnter(Collision col)
    {
        GameObject load = Resources.Load("effects/FiveshotImpact") as GameObject;
        GameObject loaded = Instantiate(load) as GameObject;

        loaded.transform.position = transform.position;

        switch (col.gameObject.name)
        {
            case "Disk":
                //damaging Disk
                col.gameObject.GetComponent<Disk>().HP -= 2;
                Destroy(gameObject, 2f);
                break;
            case "Kube":
                //damaging Kube
                col.gameObject.GetComponent<Kube>().HP -= 2;
                Destroy(gameObject, 2f);
                break;
            case "Schtryker":
                //damaging Schtryker
                col.gameObject.GetComponent<Schtryker>().HP -= 2;
                Destroy(gameObject, 2f);
                break;
            case "Skarab":
                //damaging skarab
                col.gameObject.GetComponent<Skarab>().HP -= 2;
                Destroy(gameObject, 2f);
                break;
            case "VektronWing":
                //damaging vektronWing (and thus Vektron)
                col.gameObject.GetComponent<VektronWing>().HP -= 2;
                Destroy(gameObject, 2f);
                break;
            //projectile interceptions
            case "Cield":
                //damaging cield
                col.gameObject.GetComponent<Cield>().HP -= 2;
                Destroy(gameObject, 2f);
                break;
            case "KubeProjectile":
                //loading kubeprojectile impact effect
                GameObject load2 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded2 = Instantiate(load2) as GameObject;

                loaded2.transform.position = gameObject.transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject, 4f);
                break;
            case "SkarabProjectile":
                //loading kubeprojectile impact effect
                GameObject load1 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded1 = Instantiate(load1) as GameObject;

                loaded1.transform.position = gameObject.transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject, 4f);
                break;
            case "VektronProjectile":
                //loading kubeprojectile impact effect
                GameObject load3 = Resources.Load("effects/KubeProjectileImpact") as GameObject;
                GameObject loaded3 = Instantiate(load3) as GameObject;

                loaded3.transform.position = gameObject.transform.position;

                Destroy(col.gameObject);
                Destroy(gameObject, 4f);
                break;
            case "CieldShield":
                //damaging cields shield
                col.gameObject.GetComponent<CieldShield>().HP--;
                Destroy(gameObject, 4f);
                break;
        }
    }
}
