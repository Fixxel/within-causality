﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

	public bool Shake = false;

	float shakeTimer = 0;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        //GameObject player = GameObject.Find("PlayerShip");

        //transform.LookAt(player.transform.position);

		shakeTimer -= Time.deltaTime;

		if(Shake == true){
			shakeTimer = 0.1f;
			Shake = false;
		}

		if(shakeTimer > 0){
			gameObject.transform.Rotate (Random.Range(-1f,1f),Random.Range(-1f,1f),Random.Range(-1f,1f));
		}

		else if(shakeTimer <= 0 && gameObject.transform.rotation != Quaternion.identity){
			gameObject.transform.rotation = Quaternion.identity;
		}
	}
}
