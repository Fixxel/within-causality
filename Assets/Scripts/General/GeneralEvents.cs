﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralEvents : MonoBehaviour {

    float scoreTimer;

    public int sectionCount;
    public int GameOverScore;

	// Use this for initialization
	void Start () {
        sectionCount = 0;
	}
	
	// Update is called once per frame
	void Update () {

        //giving player score over time
        if (scoreTimer > 2)
        {
            GameObject load = GameObject.Find("Score");
            load.GetComponent<Scoredisplay>().Score += 5;

            scoreTimer = 0;
        }
        
        scoreTimer += Time.deltaTime;
	}
    
    public void DropLoot(float rng, string enemy, Vector3 enemypos)
    {

        GameObject loadHP = Resources.Load("pickups/PickupBoxHP") as GameObject;

        switch (enemy)
        {
            case "Disk":
				if (rng >= 0 && rng <= 1)   //HP
				{
					GameObject hp = Instantiate(loadHP) as GameObject;
					hp.transform.position = enemypos;
				}
                break;

            case "Kube":
				if (rng >= 0 && rng <= 1)   //HP
				{
					GameObject hp = Instantiate(loadHP) as GameObject;
					hp.transform.position = enemypos;
				}
                break;

            case "Schtryker":
				if (rng >= 0 && rng <= 1)   //HP
				{
					GameObject hp = Instantiate(loadHP) as GameObject;
					hp.transform.position = enemypos;
				}
                break;

            case "Skarab":
				if (rng >= 0 && rng <= 2)   //HP
				{
					GameObject hp = Instantiate(loadHP) as GameObject;
					hp.transform.position = enemypos;
				}
                break;

            case "vektron":
                if (rng >= 0 && rng <= 1.5f)
                {
                    GameObject hp = Instantiate(loadHP) as GameObject;
                    hp.transform.position = enemypos;
                }
                break;

            case "vektronWing":
                if (rng >= 0 && rng <= 1)
                {
                    GameObject hp = Instantiate(loadHP) as GameObject;
                    hp.transform.position = enemypos;
                }
                break;

        }
    }

}