﻿using UnityEngine;
using System.Collections;

public class SkarabExplosion : MonoBehaviour
{
    float zAxisTimer = 3f;
    float velocity = -0.1f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        zAxisTimer -= Time.deltaTime;

        if (zAxisTimer <= 0)
        {
            velocity -= 10 * Time.deltaTime;
            transform.position += new Vector3(0,0,velocity * Time.deltaTime);
        }
    }
}
